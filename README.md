# Platform Tools - Architecture

## Continuous Delivery Pipeline Architecture

Below is a diagram showing a high-level overview of what a Continuous Delivery pipeline implementation could look like:

![Continuous Delivery Pipeline Architecture](./Continuous_Delivery_Pipeline_Architecture.png "Continuous Delivery Pipeline Architecture")

## Continuous Delivery vs Continuous Deployment

Below is a decision tree to help you decide on whether to implement Continuous Delivery or Continuous Deployment pipelines:

![Continuous Delivery vs Continuous Deployment](./Continuous_Delivery_vs_Continuous_Deployment.png "Continuous Delivery vs Continuous Deployment")

## Platform Tools - Architecture

![Platform Tools - Architecture](./Platform_Tools_-_Architecture.png "Platform Tools - Architecture")

## Application Security Architecture

Below is a diagram showing a high-level overview of what an Appication Security Architecture implementation could look like (including Static Application Security Testing (SAST) and Dynamic Application Security Testing (DAST)/Interactive Application Security Testing (IAST)):

![Application Security Architecture](./Application_Security_Architecture.png "Application Security Architecture")

